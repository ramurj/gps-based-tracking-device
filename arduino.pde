#include "SoftwareSerial.h"

SoftwareSerial mySerial(4,3);

int byteGPS = -1;
char bugger[300] = "";
char commandGPRS[6] = "$GPRMC";
bool connection = false;
bool data_available = false;

class latlong{
	public:
		int degree;
		int min;
		int sec;

		void input(int degreeinput, int decimelinput)
		{
			degree = degreeinput;
			sec = decimelinput%100;
			min = decimelinput/100;
		}

		int gmaps_output(){
			return (degree + (min/60) + (sec/360000));
		}
}latitude,longitude;

void readdata()
{
	int count = 0;
	int count1 = 0;

	for(int i=0;i<300;++i)
		buffer[i] = '';

	while(!data_available)
	{
		byteGPS = mySerial.read();
		if (byteGPS == -1)
			delay(100);

		else{
			buffer[count] = byteGPS;
			count++;

			if (byteGPS == 13)
			{
				count1 = 0;
				for (int i = 0; i < 7; ++i)
					if (buffer[i] == commandGPRS[i])
						count1++;
				if (count1 == 6)
				{
					data_available = true;
					break;
				}
				else
				{
					for (int i=0;i<300;i++)
						buffer[i] = '';
					
					count = 0;}
				}
			}
		}
	}
}

int getdata(int v){
	int count = 0;
	int fig = 0;

	for (int i=0;i<300;i++)
	{
		if(buffer[i] == ',' || buffer[i] == '.')
			count++;
		else if (count == v)
			fig = (fig * 10) + (buffer[i] - 48);

		if(count>v)
			break;
	}
	return fig;
}

void connect()
{
	Serial.print("AT+cgatt=1\r\n");        //Attach to GPRS Service 
    delay(2000);
    Serial.print("at+cgdcont=1,\"IP\",\"bsnlnet\"\r\n");     //Define PDP Context (cid, PDP type, APN)
    delay(2000);
    Serial.print("at+cdnscfg=\"208.67.222.222\",\"208.67.220.220\"\r\n");            //Configure Domain Name Server
    delay(2000);
    Serial.print("at+cstt=\"bsnlnet\",\"\",\"\"\r\n");       //Start Task & set APN, User ID, and 
    delay(2000);
    Serial.print("at+ciicr\r\n");           //Bring up wireless connection with GPRS (more time)
    delay(3000);   
    Serial.print("at+cifsr\r\n");           //Get Local IP address, returns it
    delay(2000);
    Serial.print("at+cipstatus\r\n");       //Get Connection Status 
    delay(2000);
    Serial.print("at+ciphead=1\r\n");       //Tells module to add an 'IP Header' to receive data 
    delay(2000);
    Serial.print("at+cdnsorip=1\r\n");      //Indicates request will be IP address (0), or domain name (1) 
    delay(2000);
    Serial.print("at+cipstart=\"TCP\",\"www.gangothri.org\",\"80\"\r\n");  //Start up TCP connection (mode, IP address/name, port) 
    delay(8000);
    Serial.print("at+cipsend\r\n");         //Issue Send Command  
    delay(1000);

    connection = true;
}

void send(latlong latitude, latlong longitude)
{
 Serial.print(" GET http://www.ramu.x10.mx/getdata.php?lat="); 
 Serial.print(latitude.gmaps_output());
 Serial.print("&longitude=");
 Serial.print(latitude.gmaps_output());
 Serial.print(" HTTP/1.1\r\nHost: www.gangothri.org\r\nConnection: Keep-Alive\r\n\r\n\032");
 Serial.print("\r\nConnection: Keep-Alive\r\n\r\n");
 data_available = false;
}

void setup()
{
	Serial.begin(9600);
	mySerial.begin(9600);
	connect();
}

void loop()
{
	readdata();
	if (data_available){
		longitude.input(getdata(3),getdata(4));
		latitude.input(getdata(6),getdata(7))
		send();
	}	
}