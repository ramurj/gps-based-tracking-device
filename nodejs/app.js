// var databaseUrl = "mydb"; // "username:password@example.com/mydb"

if(process.env.VCAP_SERVICES){
	var env = JSON.parse(process.env.VCAP_SERVICES);
	var mongo = env['mongodb-1.8'][0]['credentials'];
}
else{
	var mongo = {
		"hostname":"127.0.0.1",
		"port":27017,
		"username":"",
		"password":"",
		"name":"",
		"db":"mydb"
	}
}
var generate_mongo_url = function(obj){
	obj.hostname = (obj.hostname || 'localhost');
	obj.port = (obj.port || 27017);
	obj.db = (obj.db || 'mydb');
	if(obj.username && obj.password){
		return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
	}
	else{
		return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
	}
}
var databaseUrl = generate_mongo_url(mongo);
console.log(databaseUrl);


var portno = 8000;
var collections = ["users", "gps"];

var data = require("./data.js").data;
var user = require("./data.js").user;

var express = require("express");
var mongostore = require('connect-mongodb');

// var MemoryStore = express.session.MemoryStore;

var crypto = require("crypto");
var app = express();

data.add(-25.363882,131.044922,2,'A',1);
console.log('Data added');

app.configure(function(){
	app.use(express.bodyParser()),
	app.use(express.cookieParser()),
	app.use(express.static(__dirname + '/www/public')),
	app.use(express.session({
							store: new mongostore({url : databaseUrl}),
							// store: new MemoryStore(),
							secret: 'topsecret',
							cookie: {maxAge: 60000 * 1} // 20 minutes 
							})),

	app.get("/", function(req, res){
		res.sendfile(__dirname + '/www/index.html');
	}),
	app.post("/login", function(req, res){
		var ramu = new user(req.body.username,req.body.password);
		ramu.auth(function(err,user){
			if( !user || err) {
				console.log("user not found");
				var html = "<!doctype html>" +
				"<html><head><title>Incorrect username</title></head>" + 
				"<body>Incorrect Login</body></html>";
				res.end(html);
			}			
			else {
				console.log("user found")
				req.session.auth = user.id;
				res.sendfile(__dirname + '/www/map.html');				
		  	}
		});
	}),

	app.get("/senddata/:lat/:lang/:vel/:status/:id", function(req, res){
		console.log("saving data");
		console.log(data.set(req.params.lat,req.params.lang,req.params.vel,req.params.status,req.params.id));
		res.format({
			'text/plain': function(){
				res.send('ok' + req.params.status);
			}
		});
		console.log(data.get(1));
	}),

	app.get("/logout", function(req, res){
		delete req.session.id;
		res.redirect('/');
	}),

	app.get("/getdata", function(req, res){
		res.contentType('json');		
		res.send(data.get(req.session.auth));
	}),

	app.get("/adduser/:username/:password/:id", function(req, res){
		var ramu = new user(req.params.username,req.params.password,req.params.id);
		ramu.make(function(err,user){
			if(!err){
				var html = "<!doctype html>" +
				"<html><head><title>Result</title></head>" + 
				"<body>User successfully added</br>" + 
				"Username is " + user.username + "</br>" +
				"Password is " + user.password + "</br>" + 
				"Hashed password is " + user.hashed_password + "</br>" +
				"Salt is " + user.salt + "</br>" +
				"Id is " + user.id + " </body></html>";
				res.end(html);
			}
			else console.log('Error: ' + err);
		});
	})
});

app.listen(portno);
console.log("Server started on port " + portno);
