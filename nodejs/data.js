// var databaseUrl = "mydb"; // "username:password@example.com/mydb"
if(process.env.VCAP_SERVICES){
	var env = JSON.parse(process.env.VCAP_SERVICES);
	var mongo = env['mongodb-1.8'][0]['credentials'];
}
else{
	var mongo = {
		"hostname":"127.0.0.1",
		"port":27017,
		"username":"",
		"password":"",
		"name":"",
		"db":"mydb"
	}
}
var generate_mongo_url = function(obj){
	obj.hostname = (obj.hostname || 'localhost');
	obj.port = (obj.port || 27017);
	obj.db = (obj.db || 'mydb');
	if(obj.username && obj.password){
		return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db;
	}
	else{
		return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db;
	}
}
var databaseUrl = generate_mongo_url(mongo);

var collections = ["users", "gps"];
var db = require("mongojs").connect(databaseUrl, collections);

var crypto = require("crypto");
var sys = require("sys");

var data = new Array;

data.find = function(id){
var len = this.length;
for(var i = 0; i<len; i++)
	if(id == this[i].id) return this[i];
return new Error("ID not found");
};

data.add = function() {
	if (this.find(arguments[3]) instanceof Error){
		var p = {};
		p.lat = arguments[0];
		p.lang = arguments[1];
		p.vel = arguments[2];
		p.status = arguments[3];
		p.id = arguments[4];
		return Array.prototype.push.call(this, p);
	}
	else return new Error("Error: Cannot add ID=" + id + ", already exists");
};

data.set = function(lat,lang,vel,status,id) {
	var p = this.find(id);	
	if (p instanceof Error)	return new Error("Error: Cannot set ID=" + id + ", ID does not exist"); //console.log("Error: Cannot set ID=" + id + ", ID does not exist");

	p.lat = lat;
	p.lang = lang;
	p.vel = vel;
	p.status = status;
	return p;
};

data.get = function(id) {	
	var p = this.find(id);
	if (p instanceof Error)	return new Error("Error: Cannot get ID=" + id + ", ID does not exist");
	else return(JSON.stringify(p));
};

var User = function(username, password, id) {
	self = this;
	this.username = username;
	this.password = password;
	this.hashed_password = undefined;
	this.salt = undefined;
	this.id = id;
	console.log("New user made");
}

User.prototype = {

makesalt : function(){
	return Math.round((new Date().valueOf() * Math.random())) + '';
},

load : function(cb){
	console.log("load");
	db.users.findOne({username : self.username}, function(err, user){
		if (!user || err){
			console.log("db load error");
			//console.log('Error in loading: ' + err);
			return cb(new Error("User not found"));
		}
		else {
			console.log("db load success");
			self.hashed_password = user.password;
			self.salt = user.salt;
			self.id = user._id;
			//this.email = user.email;
			if(cb && typeof(cb) === "function") return cb(null,self);	//self is different from user as user is output of db call
		}
	});	
},

encrypt : function(password) {
	if(!password || typeof(password) != "string") password = this.password;
	return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
},

auth : function(cb) {
	console.log("auth");
	this.load(function(err,user){
		if(err) return cb(err);
		else{
			if(user.encrypt() === user.hashed_password)	return cb(null,user);	//self same as user
			else return cb(new Error("Invalid Login"));
		}
	});
},

save : function(password,cb){
	self.auth(function(err,auth){
		if(err)	return cb(err);
		else {				
			self.salt = self.makesalt();
			db.users.update({username: self.username}, {_id : self.id, username: self.username, password : self.encrypt(password), salt : self.salt}, function(err, saved){
				if( err || !saved){
					console.log("Error in updating:" + err);
					return cb(new Error("Error: Could not save user details for " + self.username));
				}
				else {
					self.password = password;
					return cb(null,self);
				}
			});
		}
	});
},

make : function(cb){
	if(!self.id) return cb(new Error('Error: id not given'));
	this.load(function(err,userl){
		if(err){
			self.salt = self.makesalt();
			console.log(self);
			db.users.save({_id : self.id, username: self.username, password : self.encrypt(), salt : self.salt}, function(err, saved){
				if(err){
					//console.log("Error in saving: " + err);
					return cb(new Error("Error: Could not save user details for " + self.username));
				} 
				else return cb(null,self);
			});
		}
		else {
			// console.log('User exists');
			// console.log(userl);
			return cb(new Error('User already exists'));
		}
	});
}
};  /* /user prototype */

exports.user = User;
exports.data = data;