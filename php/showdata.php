<?php
include './php/init.php';
	
	$veh = fRequest::get('veh','int',0,TRUE);		

	if($veh)
	try {
		$result = $db->query("SELECT * FROM live WHERE veh=$veh");
		$result->tossIfNoRows();

		$row1 = $result->fetchRow();
		$lat = $row1['lat'];
		$long = $row1['long'];
		$vel = $row1['vel'];
		$status = $row1['status'];
		$result=json_encode($row1);
		echo $result;
	} 

	catch (fNoRowsException $e) {
		fMessaging::create('error', 'index.php', 'We are still not finished adding your info to the database, Please try again later');
		fURL::redirect(SITE_NAME . 'index.php');
	}

?>