<?php
	include './php/init.php';

	fAuthorization::destroyUserInfo();
	fMessaging::create('info', 'index.php', 'You were successfully logged out');
	fURL::redirect(SITE_NAME . 'index.php');	

?>