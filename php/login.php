<?php
	include './php/init.php';

	$name = fRequest::get('username');
	$password = fRequest::get('password');
	
	try {
		$result = $db->query("SELECT * FROM user WHERE username=%s", $name);
		$result->tossIfNoRows();

		$row1 = $result->fetchRow();
		$hash = $row1['password'];

		if (fCryptography::checkPasswordHash($password, $hash)) {
		    $veh = $row1['vehicle'];		    
		    fAuthorization::setUserAuthLevel('user');
		    fAuthorization::setUserToken($veh);
		    fURL::redirect(SITE_NAME . 'map.php');		    
		}	
		else {
			fMessaging::create('error', 'index.php', 'It seems the username or password entered is invalid.');
			fURL::redirect(SITE_NAME . 'index.php');
		}
	} 
	catch (fNoRowsException $e) {
		fMessaging::create('error', 'index.php', 'It seems the username or password entered is invalid.');
		fURL::redirect(SITE_NAME . 'index.php');    
	}
	catch (fUnexpectedException $e) {
		fMessaging::create('error', 'index.php', 'There was an error connecting the database, Please try again later');
		fURL::redirect(SITE_NAME . 'index.php');    
	}
