GPS BASED TRACKING WITH ARDUINO
=========

Its the hardware and software code for a GPS based
tracking app that displays the live location of a 
tracking device plotted on Google Maps.

PHP folder
----
php contains the code for the website backend 
implemented using php and mysql. It uses the flourish
php framework.

Nodejs folder
-----------
nodejs contains the same implemented using nodejs and
mongodb. It uses the expressjs framework.

Arduino
-----------
arduino.pde contains the arduino code.